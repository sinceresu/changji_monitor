#ifndef _ERR_CODE_H
#define _ERR_CODE_H

namespace yida_mapping{
#define ERRCODE_OK 0
#define ERRCODE_MORE_DATA 1
#define ERRCODE_END_OF_STREAM 2
#define ERRCODE_NOTIMPL  -1
#define ERRCODE_NOINTERFACE  -2
#define ERRCODE_POINTER  -3
#define ERRCODE_NOT_INITIALIZED  -4
#define ERRCODE_FAILED  -5
#define ERRCODE_UNEXPECTED  -6
#define ERRCODE_ACCESSDENIED  -7
#define ERRCODE_INVALIDHANDLE  -8
#define ERRCODE_OUTOFMEMORY  -9
#define ERRCODE_INVALIDARG  -10
#define ERRCODE_TIMEOUT  -11
#define ERRCODE_BUFFER_SIZE  -12
#define ERRCODE_INIT_NETWORK  -13
#define ERRCODE_NO_ACK  -14
#define ERRCODE_EXCEPTION  -15
#define ERRCODE_NO_DATA  -16
#define ERRCODE_NOTFOUND -17

} // namespace yida_mapping

#endif

