#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include <setjmp.h>
#include <errno.h>
#include <vector>
#include <iostream>

using namespace std;
namespace yida_mapping {
namespace common {
//字符串分割函数  
void SplitString(const string& s, vector<string>& v, const string& c);
//字符串拼接函数
char* SplicingString(const char *a, const char *b);
}
}
#endif