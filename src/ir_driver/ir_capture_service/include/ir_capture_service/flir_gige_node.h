#ifndef FLIR_GIGE_NODE_H_
#define FLIR_GIGE_NODE_H_
#include  <map>

#include "ir_capture_service/flir_gige_ros.h"
#include "ir_capture_service/FlirGigeDynConfig.h"
#include "camera_base/camera_node_base.h"

#include "ir_capture_service_msgs/start_capture.h"
#include "ir_capture_service_msgs/stop_capture.h"

namespace ir_capture_service {

class FlirGigeNode : public camera_base::CameraNodeBase<FlirGigeDynConfig> {
 public:
  FlirGigeNode(const ros::NodeHandle &nh,
                        const std::string& identifier, 
                         const std::string& frame_id) 
      : CameraNodeBase(nh), flir_gige_ros_(nh, identifier, frame_id) {};

  virtual void Acquire() override;
  virtual void Setup(FlirGigeDynConfig &config) override;

 private:
  FlirGigeRos flir_gige_ros_;


};

}  // namespace ir_capture_service

#endif  // FLIR_GIGE_NODE_H_
