#ifndef IR_CAPTRUE_SERVICE_H_
#define IR_CAPTRUE_SERVICE_H_
#include  <map>
#include  <memory>

#include "ir_capture_service/flir_gige_node.h"
#include "ir_capture_service/FlirGigeDynConfig.h"
#include "camera_base/camera_node_base.h"

#include "ir_capture_service_msgs/start_capture.h"
#include "ir_capture_service_msgs/stop_capture.h"

namespace ir_capture_service {

class IRCaptureServiceNode  {
 public:
  IRCaptureServiceNode(ros::NodeHandle &nh);

 private:
    bool HandleStartCapture(
        ir_capture_service_msgs::start_capture::Request& request,
        ir_capture_service_msgs::start_capture::Response& response);
    bool HandleStopCapture(
        ir_capture_service_msgs::stop_capture::Request& request,
        ir_capture_service_msgs::stop_capture::Response& response);

    std::map<std::string, std::shared_ptr<FlirGigeNode>> flir_gige_nodes_;
    ros::NodeHandle & nh_;
	std::vector<::ros::ServiceServer> service_servers_;

};

}  // namespace ir_capture_service

#endif  // IR_CAPTRUE_SERVICE_H_
