#include "ir_capture_service/ir_capture_service.h"
#include "gflags/gflags.h"
#include "glog/logging.h"

#include "ir_capture_service/node_constants.h"

namespace ir_capture_service {
IRCaptureServiceNode::IRCaptureServiceNode(ros::NodeHandle &nh) :
nh_(nh)
{
  service_servers_.push_back(nh_.advertiseService(
      kStartCaptureServiceName, &IRCaptureServiceNode::HandleStartCapture, this));
  service_servers_.push_back(nh_.advertiseService(
      kStopCaptureServiceName, &IRCaptureServiceNode::HandleStopCapture, this));
}


bool IRCaptureServiceNode::HandleStartCapture(
    ir_capture_service_msgs::start_capture::Request& request,
    ir_capture_service_msgs::start_capture::Response& response) {
  LOG(INFO) << "Enter HandleStartCapture" ;
  if (flir_gige_nodes_.count(request.camera_id)) {
    response.status.code = 0;
    response.status.message = "camera already started!";
  LOG(WARNING) << "camera already started!" ;
    return true;
  }
  CHECK(flir_gige_nodes_.emplace(request.camera_id, std::make_shared<FlirGigeNode>(nh_, request.camera_ip, request.camera_id)).second);
  flir_gige_nodes_[request.camera_id]->Run();

  LOG(INFO) << "Exit HandleStartCapture" ;
  return true;
}

bool IRCaptureServiceNode::HandleStopCapture(
    ir_capture_service_msgs::stop_capture::Request& request,
    ir_capture_service_msgs::stop_capture::Response& response) {
  LOG(INFO) << "Enter HandleStopCapture" ;
  if (flir_gige_nodes_.count(request.camera_id) == 0) {
    response.status.code = -1;
    response.status.message = "camera not started!!";
    
    LOG(WARNING) << "camera not started!!" ;
    return false;
  }
  flir_gige_nodes_[request.camera_id]->End();
  auto it = flir_gige_nodes_.find(request.camera_id);
  flir_gige_nodes_.erase(it);

  LOG(INFO) << "Exit HandleStopCapture" ;
  return true;
}
}  // namespace ir_capture_service
