#include "gflags/gflags.h"
#include "glog/logging.h"

#include "ir_capture_service/ir_capture_service.h"

int main(int argc, char **argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;

  ros::init(argc, argv, "ir_capture_service");
  ros::NodeHandle nh("~");

  try {
    ir_capture_service::IRCaptureServiceNode capture_service_node(nh);
    // capture_service_node.Run();
    ros::spin();
    // flir_gige_node.End();
  }
  catch (const std::exception &e) {
    ROS_ERROR("%s: %s", nh.getNamespace().c_str(), e.what());
  }
}
