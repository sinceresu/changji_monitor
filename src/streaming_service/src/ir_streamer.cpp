#include "ir_streamer.h"
#include <limits>
#include <iostream>
#include <fstream>
#include <thread> 
#include <chrono> 

#include <boost/filesystem.hpp>

#include <opencv2/opencv.hpp>
#include  <opencv2/imgcodecs.hpp>

#include "ros/time.h"

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "streamer.h"
#include "common/err_code.h"


using namespace std;
using namespace cv;

namespace streaming_service {
constexpr double kTfBufferCacheTimeInSeconds = 10.;
const int kDefaultFrameRate = 5;
namespace {

void QuantizeTempertureImage( const cv::Mat& temperture_image, cv::Mat& quantized_img, float min_celcius, float max_celcius) {
  const float alpha = std::numeric_limits<uint16_t>::max() / (max_celcius - min_celcius);
  const float beta = -alpha * min_celcius;
  temperture_image.convertTo(quantized_img, CV_16UC1, alpha, beta);
    
}

// void ConvertQuantizedImageToYuv422(const cv::Mat& quantized_img, vector<cv::Mat> & yuv422_img) {
//   cv::Mat& y_img = yuv422_img[0];
//   cv::Mat& u_img = yuv422_img[1];
//   cv::Mat& v_img = yuv422_img[2];

//   for (size_t row= 0; row < quantized_img.rows; row++) {
//     for (size_t col= 0; col < quantized_img.cols; col += 2) {
//       uint16_t value1 = quantized_img.at<uint16_t>(row, col);
//       y_img.at<uint8_t>(row, col) = static_cast<uint8_t>(value1 & 0xff);
//       u_img.at<uint8_t>(row, col/2) = static_cast<uint8_t>((value1 >>8) & 0xff);
//       uint16_t value2 = quantized_img.at<uint16_t>(row, col + 1);
//       y_img.at<uint8_t>(row, col + 1) = static_cast<uint8_t>(value2 & 0xff);
//       v_img.at<uint8_t>(row, col/2) = static_cast<uint8_t>((value2 >>8) & 0xff);
//     }
//   }
// }
void SplitUint16(uint16_t input, uint8_t &even_byte, uint8_t &odd_byte) {
  even_byte = 0u;
  odd_byte = 0u;
  for (size_t i = 0; i < 8; i++) {
    uint8_t even_bit =((1u << (i * 2)) & input ) != 0u ? 1u : 0u;
    uint8_t odd_bit = ((1u << (i * 2 + 1)) & input )  != 0u ? 1u : 0u;
    even_byte |= even_bit << i;
    odd_byte |= odd_bit << i;
  }
}

void ConvertQuantizedImageToYuv422(const cv::Mat& quantized_img, vector<cv::Mat> & yuv422_img) {
  cv::Mat& y_img = yuv422_img[0];
  cv::Mat& u_img = yuv422_img[1];
  cv::Mat& v_img = yuv422_img[2];

  for (size_t row= 0; row < quantized_img.rows; row++) {
    for (size_t col= 0; col < quantized_img.cols; col += 2) {

      uint16_t value1 = quantized_img.at<uint16_t>(row, col);
      uint8_t even_byte;
      uint8_t odd_byte;
      SplitUint16(value1, even_byte, odd_byte);
      y_img.at<uint8_t>(row, col) = even_byte;
      u_img.at<uint8_t>(row, col/2) = odd_byte;

      uint16_t value2 = quantized_img.at<uint16_t>(row, col + 1);
      SplitUint16(value2, even_byte, odd_byte);
      y_img.at<uint8_t>(row, col + 1) = even_byte;
      v_img.at<uint8_t>(row, col/2) = odd_byte;
    }
  }
}
}

IRStreamer::IRStreamer(const std::string& camera_id) :
  camera_id_(camera_id)

 {

}

IRStreamer::~IRStreamer() {
  Stop();
 }

void IRStreamer::SetParameters(int streaming_port, float min_celcius, float  max_celcius, int key_frame_interval, int constant_rate_factor) {
  streaming_port_ = streaming_port;
  min_celcius_ = min_celcius;
  max_celcius_ = max_celcius;
  key_frame_interval_ = key_frame_interval;
  constant_rate_factor_ = constant_rate_factor;

}

bool IRStreamer::Start() {
 LOG(INFO) << "IRStreamer::Start";

  input_ready_ = false;
  stop_flag_ = false;
  initialized_ = false;

  work_thread_ = thread(bind(&IRStreamer::Run, this));

  return true;
}

void IRStreamer::Stop() {
  LOG(INFO) << "IRStreamer::Stop";

  stop_flag_ = true;
  work_thread_.join();

}

void IRStreamer::SendPosedImage( const Eigen::Affine3d& pose, uint64_t image_time, cv::Mat image) {
	  // ROS_INFO("HandleSyncMessage");
  {
    unique_lock<std::mutex> lock(mutex_);
    input_frame_ = move(image);
    input_frame_time_ = image_time;
    input_pose_ = pose;

    input_ready_ = true;
  }
  // uint64_t timestamp = img_msg->image.header.stamp.toNSec() / 1000000;

  // SendPosedImage(cv_ptr->image, pose, timestamp);
}

void IRStreamer::StreamingPosedImage() {

  cv::Mat quantized_img;
  Eigen::Affine3d pose;
  {
    unique_lock<std::mutex> lock(mutex_);
    if (!input_ready_) 
      return;

    if (!initialized_) {
      int ret = Initialize(input_frame_.size(), input_frame_time_);
      if (ERRCODE_OK != ret)
        return;
    }
    QuantizeTempertureImage(input_frame_, quantized_img, min_celcius_, max_celcius_);
    pose = input_pose_;
    input_ready_ = false;
  }

  ConvertQuantizedImageToYuv422(quantized_img, yuv422_img_);

   if (ERRCODE_OK != streamer_->Send(yuv422_img_, pose, input_frame_time_ - start_timestamp_)) {
  //if (ERRCODE_OK != streamer_->Send(yuv422_img_, pose, timestamp_ms)) {
    LOG(WARNING) << "Failed to streaming!";
  }
  
}

int IRStreamer::Initialize(const cv::Size& image_size, uint64_t start_time_ms){
  InitializeImages(image_size);

  streamer_ = unique_ptr<Streamer>(new Streamer());
  streamer_->SetEncodeParam(key_frame_interval_, constant_rate_factor_);

  stringstream streaming_url;
  streaming_url << "rtsp://localhost:" << streaming_port_<< "/live/" <<  camera_id_;
  if (0 != streamer_->Initialize(image_size.width, image_size.height, streaming_url.str(), kDefaultFrameRate, start_time_ms)) {
    LOG(WARNING) << "Failed to initialize rtsp pusher!";
    return ERRCODE_INIT_NETWORK;
  }

  start_timestamp_ = start_time_ms;
  initialized_ = true;
  return ERRCODE_OK;

}
void IRStreamer::InitializeImages(const cv::Size& image_size){
  yuv422_img_ = std::vector<cv::Mat>(3);
  yuv422_img_[0]  = Mat(image_size.height,  image_size.width, CV_8UC1,  Scalar(0));
  yuv422_img_[1]  = Mat(image_size.height,  image_size.width / 2, CV_8UC1, Scalar(128));
  yuv422_img_[2]  = Mat(image_size.height,  image_size.width / 2, CV_8UC1, Scalar(128));
}

void IRStreamer::Run() {
  // chrono::duration sleep_ms = chrono::milliseconds(1000 / kDefaultFrameRate);
  LOG(INFO) << "Enter IRStreamer work thead!";
  Eigen::Affine3d invalid_pose;
  invalid_pose.translation() = Eigen::Vector3d(100000.0f, 100000.0f, 100000.0f);
  while(!stop_flag_) {
      if (input_ready_) {
        StreamingPosedImage();
        continue;
      }

      if (initialized_) {
        uint64_t timestamp = ros::Time::now().toNSec() / 1000000;
        if (ERRCODE_OK != streamer_->Send(yuv422_img_, invalid_pose, timestamp - start_timestamp_)) {
          LOG(WARNING) << "Failed to streaming!";
        }
      }
      this_thread::sleep_for(chrono::milliseconds(1000 / kDefaultFrameRate));
    
  }
  LOG(INFO) << "Exit IRStreamer work thead!";

}

}

