#pragma once

#include <string>
#include <memory>
#include <vector>
#include <mutex>
#include <thread>


#include <opencv2/opencv.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>



namespace  streaming_service{
class Streamer;

class IRStreamer {
public: 
  IRStreamer(const std::string& camera_id = "");
  ~IRStreamer();

  IRStreamer(const IRStreamer&) = delete;
  IRStreamer& operator=(const IRStreamer&) = delete;
  void SetParameters(int streaming_port, float min_celcius, float  max_celcius, int key_frame_interval, int constant_rate_factor);
  bool Start();
  void Stop();

  void SendPosedImage( const Eigen::Affine3d& pose, uint64_t image_time_ms, cv::Mat image);

  private:
    void StreamingPosedImage();
    void InitializeImages(const cv::Size& image_size);
    int Initialize(const cv::Size& image_size, uint64_t start_time_ms);   

    void Run();






    std::string camera_id_;

    int streaming_port_;
    float min_celcius_;
    float max_celcius_;

    std::unique_ptr<Streamer> streamer_;
    cv::Mat quantized_img_;
    std::vector<cv::Mat> yuv422_img_;
    uint64_t start_timestamp_;
    int key_frame_interval_;
    int constant_rate_factor_;

    bool initialized_;

    bool stop_flag_;

    std::mutex mutex_;
    bool input_ready_; 

    cv::Mat input_frame_;
    uint64 input_frame_time_;

    Eigen::Affine3d input_pose_;
    std::thread work_thread_;
  };
  
}