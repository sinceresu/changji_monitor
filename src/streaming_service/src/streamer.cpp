#include "streamer.h"
#include <limits>
#include <iostream>
#include <fstream>
#include <thread> 

#include <opencv2/opencv.hpp>
#include  <opencv2/imgcodecs.hpp>

extern "C" {
//#include "libavutil/avutil.h"
#include "libavcodec/avcodec.h"

#include "libavformat/avformat.h"
//新版里的图像转换结构需要引入的头文件
#include "libswscale/swscale.h"
#include "libavutil/opt.h"
};

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "common/err_code.h"



using namespace std;
using namespace cv;

namespace streaming_service {
namespace {
	static void log_callback_help(void *ptr, int level, const char *fmt, va_list vl)
	{
		if (level > AV_LOG_WARNING)
			return;
		char output[512];
		vsnprintf(output, sizeof(output), fmt, vl);
		LOG(INFO) << output;
		//sprintf(output, fmt, vl);
	}
}

Streamer::Streamer() :
  codec_name_("libx264"),
  bsf_(NULL),
  pkt_(NULL),
  frame_(NULL),
  key_frame_interval_(10),
  constant_rate_factor_(20)
{
  // av_register_all();
  avformat_network_init();
  av_log_set_level(AV_LOG_VERBOSE);
	av_log_set_callback(log_callback_help);

}

Streamer::~Streamer() {

 }

int Streamer:: Initialize(int image_width, int image_height, const string& streaming_url, int frame_rate, uint64_t start_time_ms)
 {
    /* find the mpeg1video encoder */
  codec = avcodec_find_encoder_by_name(codec_name_.c_str());
	if (NULL == codec)
	{
		return ERRCODE_FAILED;
	}

	enc_ctx_ = avcodec_alloc_context3(codec);
	if (NULL == enc_ctx_) {
		return ERRCODE_FAILED;
  }

	enc_ctx_->opaque = this;

	enc_ctx_->thread_count = 0;
  enc_ctx_->width = image_width;
  enc_ctx_->height = image_height;
  enc_ctx_->framerate = (AVRational){frame_rate, 1};
  enc_ctx_->time_base= (AVRational){1,1000};
	// enc_ctx_->pix_fmt = AV_PIX_FMT_YUV444P;
	enc_ctx_->pix_fmt = AV_PIX_FMT_YUV422P;
  enc_ctx_->gop_size = key_frame_interval_;   //图像组两个关键帧（I帧）的距离
  enc_ctx_->max_b_frames = 0;



  // enc_ctx_->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;   //添加PPS、SPS
  // enc_ctx_->qmin = 1;
  // enc_ctx_->qmax = 30;
  // enc_ctx_->bit_rate = 500000;
  // enc_ctx_->bit_rate_tolerance = enc_ctx_->bit_rate * 2;

  AVDictionary* opts = NULL;
  av_dict_set(&opts, "preset", "slow", 0);
  av_dict_set_int(&opts, "crf", constant_rate_factor_, 0);

  // av_dict_set(&opts, "x264opts", "rc_method=2:vbv-maxrate=728:vbv-bufsize=364", 0);

    /* open it */
  if (avcodec_open2(enc_ctx_, codec, &opts) < 0) {
      fprintf(stderr, "Could not open codec\n");
		  return ERRCODE_FAILED;
  }

	bsf_ =  av_bsf_get_by_name("h264_metadata");   
  if (NULL == bsf_) {
      fprintf(stderr, "Could not av_bitstream_filter_init\n");
      return ERRCODE_FAILED; 
  }


int ret = avformat_alloc_output_context2(&ofmt_ctx_,NULL, "rtsp", streaming_url.c_str()); 
if (ret < 0) {
  printf("Error occurred when opening output URL\n");
  return ERRCODE_FAILED;
}

video_st_ =avformat_new_stream(ofmt_ctx_, codec);

  if (!video_st_) {

    printf("Failed allocating output stream\n");
    return ERRCODE_FAILED;
  }
  ret = avcodec_parameters_from_context(video_st_->codecpar, enc_ctx_);
  if (ret < 0) {
    printf( "Failed to copy context from input to output stream codec context\n");
    return ERRCODE_FAILED;
  }
  video_st_->codecpar->codec_tag = 0;
  ofmt_ctx_->oformat->flags &= ~AVFMT_GLOBALHEADER;
  // if (ofmt_ctx_->oformat->flags & AVFMT_GLOBALHEADER)
	// 		enc_ctx_->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
  video_st_->time_base = (AVRational){1,1000};
  // video_st_->codec = enc_ctx_;
    
  video_st_->id = ofmt_ctx_->nb_streams-1;

  ret =  av_bsf_alloc(bsf_, &bsf_ctx_) ;
  if (ret < 0){
    fprintf(stderr, "Error during av_bsf_alloc\n");
    return ERRCODE_FAILED;
  }  
  ret = avcodec_parameters_copy(bsf_ctx_->par_in, video_st_->codecpar);
  av_bsf_init(bsf_ctx_);

    //打开输出URL（Open output URL） 
  AVOutputFormat *ofmt= ofmt_ctx_->oformat;

  av_dump_format(ofmt_ctx_, 0, streaming_url.c_str(), 1);
  if (!(ofmt->flags& AVFMT_NOFILE)) {

    ret= avio_open(&ofmt_ctx_->pb, streaming_url.c_str(), AVIO_FLAG_WRITE);

    if (ret < 0) {
      printf("Could not open output URL '%s'", streaming_url.c_str());
      return ERRCODE_FAILED;
    }

  }
  ofmt_ctx_->start_time_realtime = start_time_ms * 1000; //ms to us

  ret= avformat_write_header(ofmt_ctx_, NULL);

  if (ret < 0) {
    printf("Error occurred when opening output URL\n");
    return ERRCODE_FAILED;
  }
  frame_ = av_frame_alloc();  
  frame_->format = enc_ctx_->pix_fmt;
  frame_->width = enc_ctx_->width;
  frame_->height = enc_ctx_->height;

  ret = av_frame_get_buffer(frame_, 0);
  if (ret < 0) {
      fprintf(stderr, "Could not allocate the video frame_ data\n");
      return ERRCODE_FAILED;
  } 

	pkt_ = av_packet_alloc();

  stream_url_ = streaming_url;
  // AVFormatContext *ac[] = {ofmt_ctx_};
  // char sdp_buffer[1024];
  // av_sdp_create(ac, 1, sdp_buffer, 1024);
  // std::ofstream  sdp_file("~/test.sdp");
  // sdp_file << std::string(sdp_buffer);
  // sdp_file.close();
  return ERRCODE_OK;

 }

void Streamer::Release() {

  if (ofmt_ctx_&& !(ofmt_ctx_->oformat->flags & AVFMT_NOFILE))
    avio_close(ofmt_ctx_->pb);
  avformat_free_context(ofmt_ctx_);
  if (pkt_)
    av_packet_free(&pkt_);
  if (frame_)
    av_frame_free(&frame_);

  avcodec_free_context(&enc_ctx_);
    if (NULL != bsf_) {
		// av_free(bsf_);
		bsf_ = NULL;
	}
}
void Streamer::SetEncodeParam(int key_frame_interval, int constant_rate_factor) {
  key_frame_interval_ = key_frame_interval;
  constant_rate_factor_ = constant_rate_factor;

}
const vector<string> sei_str{"086f3693-b7b3-4f2c-9653-21492feee5b8+LiuQiHelloWorld", "086f3693-b7b3-4f2c-9653-21492feee5b8+HelloWorldIamHear"};

int Streamer::Send(const std::vector<cv::Mat>& image_yuv422, const Eigen::Affine3d& pose, int64_t timestamp_ms) {
  static int idx = 0;
   if (!frame_) {
        fprintf(stderr, "Could not allocate video frame_\n");
        return ERRCODE_FAILED;
    }
  int ret = av_frame_make_writable(frame_);
  if (ret < 0)
    return ERRCODE_FAILED;
  for(size_t y=0;y<enc_ctx_->height;y++) {
    memcpy( &frame_->data[0][y * frame_->linesize[0]], (uint8_t*)image_yuv422[0].ptr<uint8_t>(y), frame_->width*sizeof(uint8_t));
    memcpy( &frame_->data[1][y * frame_->linesize[1]], (uint8_t*)image_yuv422[1].ptr<uint8_t>(y), frame_->width/2*sizeof(uint8_t));
    memcpy( &frame_->data[2][y * frame_->linesize[2]], (uint8_t*)image_yuv422[2].ptr<uint8_t>(y), frame_->width/2*sizeof(uint8_t));
  }

  frame_->pts = timestamp_ms;

  ret = avcodec_send_frame(enc_ctx_, frame_);
  if (ret < 0) {
      fprintf(stderr, "Error sending a frame_ for encoding\n");
      return ERRCODE_FAILED;
  }
  while (ret >= 0) {
    	av_init_packet(pkt_);
      ret = avcodec_receive_packet(enc_ctx_, pkt_);
      if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
        break;
      else if (ret < 0) {
          fprintf(stderr, "Error during encoding\n");
          return ERRCODE_FAILED;
      }

	    pkt_->pts = av_rescale_q(pkt_->pts, enc_ctx_->time_base, video_st_->time_base);
	    pkt_->dts = av_rescale_q(pkt_->dts, enc_ctx_->time_base, video_st_->time_base);

	    pkt_->stream_index = video_st_->index;
      //  if (enc_ctx_->coded_frame->key_frame)
      //     pkt_->flags |= AV_PKT_FLAG_KEY;
      string sei_content = GenerateSEIHead(pose);

      av_opt_set(bsf_ctx_->priv_data, "sei_user_data", sei_content.c_str(), AV_OPT_SEARCH_CHILDREN);
      int result = av_bsf_send_packet(bsf_ctx_, pkt_);
      if (result < 0) {
            fprintf(stderr, "Error during av_bsf_send_packet\n");
            return ERRCODE_FAILED;
      }

      while ((result =av_bsf_receive_packet(bsf_ctx_, pkt_)) >= 0) {
        result = av_write_frame(ofmt_ctx_, pkt_);
        if (result < 0) {
            char error[256];
            av_make_error_string(error, 256, result);
            fprintf(stderr, "Error occurred: %s\n", error);
        }
        av_packet_unref(pkt_);
      } 
      if (result == AVERROR(EAGAIN))
        result = 0;
  }

  idx++;
  return ERRCODE_OK;
}
const string sei_head = "086f3693-b7b3-4f2c-9653-21492feee5b8+";

std::string Streamer::GenerateSEIHead(const Eigen::Affine3d& pose) {
  std::stringstream sei_content;
  Eigen::Quaterniond rotation(pose.linear());
  const auto& translation = pose.translation();
  sei_content << translation.x() << ","<< translation.y() << ","<< translation.z() << ","<< rotation.x() << ","<< rotation.y() << ","<< rotation.z() << ","<<rotation.w();

  return sei_head +  sei_content.str();
}
}

