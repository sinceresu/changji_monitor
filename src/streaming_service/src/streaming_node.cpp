#include "streaming_node.h"
#include <limits>
#include <iostream>
#include <fstream>
#include <thread> 
#include <chrono> 

#include <boost/filesystem.hpp>

#include <opencv2/opencv.hpp>
#include  <opencv2/imgcodecs.hpp>



#include "ros/package.h"
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include "tf2_eigen/tf2_eigen.h"

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "streaming_service/node_constants.h"

#include "ir_streamer.h"
#include "common/err_code.h"


using namespace std;
using namespace cv;

namespace streaming_service {
constexpr double kTfBufferCacheTimeInSeconds = 10.;
namespace {
constexpr int kLatestOnlyPublisherQueueSize = 1;

// Subscribes to the 'topic' for 'trajectory_id' using the 'node_handle' and
// calls 'handler' on the 'node' to handle messages. Returns the subscriber.
template <typename MessageType>
::ros::Subscriber SubscribeWithHandler(
    void (StreamingNode::*handler)(const std::string&,
                                    const typename MessageType::ConstPtr&),
      const std::string& topic,
    ::ros::NodeHandle* const node_handle, StreamingNode* const node) {
  return node_handle->subscribe<MessageType>(
      topic, kLatestOnlyPublisherQueueSize,
      boost::function<void(const typename MessageType::ConstPtr&)>(
          [node, handler,
           topic](const typename MessageType::ConstPtr& msg) {
            (node->*handler)(topic, msg);
          }));
}

}

StreamingNode::StreamingNode()

 {
  GetParameters();

  service_servers_.push_back(node_handle_.advertiseService(
      kStartStreamingServiceName, &StreamingNode::HandleStartStreaming, this));

  service_servers_.push_back(node_handle_.advertiseService(
      kStopStreamingServiceName, &StreamingNode::HandleStopStreaming, this));

}

StreamingNode::~StreamingNode() {
 }

void StreamingNode::GetParameters() {

  ::ros::NodeHandle private_handle("~");
  
  private_handle.param<std::string>("/streaming_service/undistort_topic", undistort_topic_, "/infrared/undistorted");

  private_handle.param<int>("/streaming_service/streaming_port", streaming_port_,  8554);
 
  private_handle.param<float>("/streaming_service/min_celcius", min_celcius_,  0.0f);
  private_handle.param<float>("/streaming_service/max_celcius", max_celcius_,  800.0f);

  private_handle.param<int>("/streaming_service/key_frame_interval", key_frame_interval_,  10);
  private_handle.param<int>("/streaming_service/constant_rate_factor", constant_rate_factor_,  20);

}

bool StreamingNode::HandleStartStreaming(
        streaming_service_msgs::start_streaming::Request& request,
        streaming_service_msgs::start_streaming::Response& response) {
  ROS_INFO("HandleStartStreaming");

  {
    std::unique_lock<std::mutex> lock(mutex_);
    if (ir_streamers_.count(request.camera_id) != 0) {
      response.status.code = StreamingResult::SUCCESS;
      response.status.message = "streaming started.";
      LOG(WARNING) << "streaming started! " ;
      return true;  
    }
    ir_streamers_[request.camera_id] = make_shared<IRStreamer>(request.camera_id);
    ir_streamers_[request.camera_id]->SetParameters(streaming_port_, min_celcius_, max_celcius_, key_frame_interval_, constant_rate_factor_);
    ir_streamers_[request.camera_id]->Start();

  }

  posed_image_subscriber_ =  SubscribeWithHandler<undistort_service_msgs::PosedImage>(
          &StreamingNode::OnPosedImage, undistort_topic_, &node_handle_,
          this);
  response.status.code = StreamingResult::SUCCESS;
  state_ = StreamingState::STREAMINGING;

  return true;
}

bool StreamingNode::HandleStopStreaming(
        streaming_service_msgs::stop_streaming::Request& request,
        streaming_service_msgs::stop_streaming::Response& response) {
  ROS_INFO("HandleStopStreaming");
  bool ir_streamer_empty = false;
  {
    std::unique_lock<std::mutex> lock(mutex_);
    if (ir_streamers_.count(request.camera_id) == 0) {
      response.status.code = StreamingResult::SUCCESS;
      response.status.message = "not streaming.";
      LOG(WARNING) << "not streaming! " ;
      return true;  
    }
    ir_streamers_[request.camera_id]->Stop();
    ir_streamers_.erase(request.camera_id);
    ir_streamer_empty = ir_streamers_.empty();
  }

  if (ir_streamer_empty) {
    posed_image_subscriber_.shutdown();
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
    state_ = StreamingState::IDLE;
  }

  response.status.code = StreamingResult::SUCCESS;
  return true;
}

void StreamingNode::OnPosedImage( const std::string& topic, const undistort_service_msgs::PosedImage::ConstPtr& img_msg) {
	  // ROS_INFO("HandleSyncMessage");
{
    std::unique_lock<std::mutex> lock(mutex_);
    if (ir_streamers_.count(img_msg->image.header.frame_id) == 0) {
      return;
    }
}
  cv_bridge::CvImageConstPtr cv_ptr= cv_bridge::toCvCopy(img_msg->image, sensor_msgs::image_encodings::TYPE_32FC1);
  Eigen::Affine3d pose;
  Eigen::fromMsg (img_msg->pose, pose);
  uint64_t timestamp = img_msg->image.header.stamp.toNSec() / 1000000;
  {
    unique_lock<std::mutex> lock(mutex_);
    ir_streamers_[img_msg->image.header.frame_id]->SendPosedImage(pose, timestamp, move(cv_ptr->image));
  }
}

}

