#pragma once

#include <string>
#include <memory>
#include <vector>
#include <mutex>
#include <thread>


#include <opencv2/opencv.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "ros/ros.h"
#include <sensor_msgs/Image.h>

#include "streaming_service_msgs/start_streaming.h"
#include "streaming_service_msgs/stop_streaming.h"
#include "undistort_service_msgs/PosedImage.h"



namespace  streaming_service{
class IRStreamer;

class StreamingNode {
 public:
    enum StreamingResult {
        SUCCESS = 0,
        IO_ERROR = -2,
        INVALID_PARAM = -4,
        ERROR = -5
    };
  enum StreamingAction {
        START = 0,
        STOP = 1,
  };
  StreamingNode();
  ~StreamingNode();

  StreamingNode(const StreamingNode&) = delete;
  StreamingNode& operator=(const StreamingNode&) = delete;

  private:
    enum StreamingState {
        IDLE = 0,
        STREAMINGING = 1,
    };

    void GetParameters();
    bool HandleStartStreaming(
        streaming_service_msgs::start_streaming::Request& request,
        streaming_service_msgs::start_streaming::Response& response);
        
    bool HandleStopStreaming(
        streaming_service_msgs::stop_streaming::Request& request,
        streaming_service_msgs::stop_streaming::Response& response);
     
    void OnPosedImage( const std::string& topic, const undistort_service_msgs::PosedImage::ConstPtr& image_msg);


    ::ros::NodeHandle node_handle_;

    std::vector<::ros::ServiceServer> service_servers_;

    ::ros::Subscriber posed_image_subscriber_;

    int state_ = StreamingState::IDLE;

    std::string undistort_topic_;
    
    std::map<std::string, std::shared_ptr<IRStreamer>> ir_streamers_;

    int streaming_port_;
    float min_celcius_;
    float max_celcius_;
    int key_frame_interval_;
    int constant_rate_factor_;
    std::mutex mutex_;


  };
  
}