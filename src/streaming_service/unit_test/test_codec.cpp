
#include <stdio.h>
#include <vector>
#include <random>
#include <memory>

#include <opencv2/opencv.hpp>
#include  <opencv2/imgcodecs.hpp>


#include "gflags/gflags.h"
#include "glog/logging.h"

#include <sensor_msgs/Image.h>

DEFINE_string(topics, "",

              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");



using namespace std;
using namespace cv;

namespace undistort_service{
namespace {
void GenRanImage(Mat& raw_image, float min_value, float max_value) {
   std::random_device rd;
    std::default_random_engine eng(rd());
    std::uniform_real_distribution<float> distr(min_value, max_value);
  for (size_t row= 0; row < raw_image.rows; row++) {
    for (size_t col= 0; col < raw_image.cols; col++) {
      raw_image.at<float>(row, col) = distr(eng);
    }
  }
}

void QuantizeTempertureImage( const cv::Mat& temperture_image, cv::Mat& quantized_img, float min_celcius, float max_celcius) {
  const float alpha = std::numeric_limits<uint16_t>::max() / (max_celcius - min_celcius);
  const float beta = -alpha * min_celcius;
  temperture_image.convertTo(quantized_img, CV_16UC1, alpha, beta);
    
}

// void ConvertQuantizedImageToYuv422(const cv::Mat& quantized_img, vector<cv::Mat> & yuv422_img) {
//   cv::Mat& y_img = yuv422_img[0];
//   cv::Mat& u_img = yuv422_img[1];
//   cv::Mat& v_img = yuv422_img[2];

//   for (size_t row= 0; row < quantized_img.rows; row++) {
//     for (size_t col= 0; col < quantized_img.cols; col += 2) {
//       uint16_t value1 = quantized_img.at<uint16_t>(row, col);
//       y_img.at<uint8_t>(row, col) = static_cast<uint8_t>(value1 & 0xff);
//       u_img.at<uint8_t>(row, col/2) = static_cast<uint8_t>((value1 >>8) & 0xff);
//       uint16_t value2 = quantized_img.at<uint16_t>(row, col + 1);
//       y_img.at<uint8_t>(row, col + 1) = static_cast<uint8_t>(value2 & 0xff);
//       v_img.at<uint8_t>(row, col/2) = static_cast<uint8_t>((value2 >>8) & 0xff);
//     }
//   }
// }
void SplitUint16(uint16_t input, uint8_t &even_byte, uint8_t &odd_byte) {
  even_byte = 0u;
  odd_byte = 0u;
  for (size_t i = 0; i < 8; i++) {
    uint8_t even_bit =((1u << (i * 2)) & input ) != 0u ? 1u : 0u;
    uint8_t odd_bit = ((1u << (i * 2 + 1)) & input )  != 0u ? 1u : 0u;
    even_byte |= even_bit << i;
    odd_byte |= odd_bit << i;
  }
}

void ConvertQuantizedImageToYuv422(const cv::Mat& quantized_img, vector<cv::Mat> & yuv422_img) {
  cv::Mat& y_img = yuv422_img[0];
  cv::Mat& u_img = yuv422_img[1];
  cv::Mat& v_img = yuv422_img[2];

  for (size_t row= 0; row < quantized_img.rows; row++) {
    for (size_t col= 0; col < quantized_img.cols; col += 2) {

      uint16_t value1 = quantized_img.at<uint16_t>(row, col);
      uint8_t even_byte;
      uint8_t odd_byte;
      SplitUint16(value1, even_byte, odd_byte);
      y_img.at<uint8_t>(row, col) = even_byte;
      u_img.at<uint8_t>(row, col/2) = odd_byte;

      uint16_t value2 = quantized_img.at<uint16_t>(row, col + 1);
      SplitUint16(value2, even_byte, odd_byte);
      y_img.at<uint8_t>(row, col + 1) = even_byte;
      v_img.at<uint8_t>(row, col/2) = odd_byte;
    }
  }
}
void DequantizeTempertureImage( const cv::Mat& quantized_img, cv::Mat& temperture_image, double min_celcius, double max_celcius) {
  const double alpha_back = (max_celcius - min_celcius) / std::numeric_limits<uint16_t>::max();
  const double beta_back =  min_celcius;
  quantized_img.convertTo(temperture_image, CV_32FC1, alpha_back, beta_back);
    
}
uint16_t ShuffleByte(uint8_t even_byte, uint8_t odd_byte) {
  uint16_t result = 0u;
  for (size_t i = 0; i < 8; i++) {
    uint8_t combined = (even_byte & 0x1u ) + ((odd_byte & 0x1u ) << 1);
    result |= combined << (i * 2);
    even_byte = even_byte >> 1;
    odd_byte = odd_byte >> 1;
  }
  return result;
}
void ConvertYuv422ToQuantizedImage(const vector<cv::Mat> & yuv422_img, cv::Mat& quantized_img) {
  const cv::Mat& y_img = yuv422_img[0];
  const cv::Mat& u_img = yuv422_img[1];
  const cv::Mat& v_img = yuv422_img[2];


  for (size_t row= 0; row < quantized_img.rows; row++) {
    for (size_t col= 0; col < quantized_img.cols; col += 2) {
      // uint16_t value1 = (u_img.at<uint8_t>(row, col/2) << 8) + y_img.at<uint8_t>(row, col);
      quantized_img.at<uint16_t>(row, col) = ShuffleByte(y_img.at<uint8_t>(row, col), u_img.at<uint8_t>(row, col/2));
      // uint16_t value2 = (v_img.at<uint8_t>(row, col/2) << 8) + y_img.at<uint8_t>(row, col + 1);
      quantized_img.at<uint16_t>(row, col + 1) = ShuffleByte(y_img.at<uint8_t>(row, col + 1), v_img.at<uint8_t>(row, col/2));
    }
  }

}
void TestCodec(int argc, char** argv) {

  vector<int> compression_params;
  compression_params.push_back(IMWRITE_PNG_COMPRESSION);
  compression_params.push_back(9);
  Mat raw_image(480, 640, CV_32FC1);
  const float raw_min = 0.f;
  const float raw_max = 800;

  GenRanImage(raw_image, raw_min, raw_max);
  
  double minVal; 
  double maxVal; 
  Point minLoc; 
  Point maxLoc;
  minMaxLoc( raw_image, &minVal, &maxVal, &minLoc, &maxLoc );
  LOG(INFO) << " input image value min: " << minVal << " max: " << maxVal;

  cv::Mat quantized_img;
  QuantizeTempertureImage(raw_image, quantized_img, raw_min, raw_max);
  std::vector<cv::Mat> yuv422_img;
  yuv422_img = std::vector<cv::Mat>(3);
  yuv422_img[0]  = Mat(raw_image.rows,  raw_image.cols, CV_8UC1,  Scalar(0));
  yuv422_img[1]  = Mat(raw_image.rows,  raw_image.cols / 2, CV_8UC1, Scalar(128));
  yuv422_img[2]  = Mat(raw_image.rows,  raw_image.cols / 2, CV_8UC1, Scalar(128));
  ConvertQuantizedImageToYuv422(quantized_img, yuv422_img);

  cv::Mat decoded_quantized_img(raw_image.rows,  raw_image.cols, CV_16UC1);

  ConvertYuv422ToQuantizedImage(yuv422_img, decoded_quantized_img);
  
  cv::Mat temp_image;
  DequantizeTempertureImage(decoded_quantized_img, temp_image, raw_min, raw_max);
  
  Mat diff_image = cv::abs(raw_image - temp_image);

  minMaxLoc( diff_image, &minVal, &maxVal, &minLoc, &maxLoc );
  LOG(INFO) << "max accuracy lost after codec: " << maxVal;

}

}
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  LOG(INFO) << "start test codec.";

  undistort_service::TestCodec(argc, argv);

  getchar();
 
}

