
#include <stdio.h>
#include <vector>
#include <thread>

#include <opencv2/opencv.hpp>


#include "gflags/gflags.h"
#include "glog/logging.h"

#include "ros/ros.h"
#include "rosbag/bag.h"
#include "rosbag/view.h"
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>

#include "../src/streamer.h"


DEFINE_string(streaming_url, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");
DEFINE_string(bag_filename, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");

DEFINE_double(min_celcius, 0., "minimum temprature.");
DEFINE_double(max_celcius, 800., "Bag to streaming.");


using namespace std;
using namespace cv;

namespace streaming_service{
namespace {

void QuantizeTempertureImage( const cv::Mat& temperture_image, cv::Mat& quantized_img, double min_celcius, double max_celcius) {
  const double alpha = std::numeric_limits<uint16_t>::max() / (max_celcius - min_celcius);
  const double beta = -alpha * min_celcius;
  temperture_image.convertTo(quantized_img, CV_16UC1, alpha, beta);
    
}

void ConvertQuantizedImageToYuv422(const cv::Mat& quantized_img, vector<cv::Mat> & yuv422_img) {
  cv::Mat& y_img = yuv422_img[0];
  cv::Mat& u_img = yuv422_img[1];
  cv::Mat& v_img = yuv422_img[2];

  for (size_t row= 0; row < quantized_img.rows; row++) {
    for (size_t col= 0; col < quantized_img.cols; col += 2) {
      uint16_t value1 = quantized_img.at<uint16_t>(row, col);
      y_img.at<uint8_t>(row, col) = static_cast<uint8_t>(value1 & 0xff);
      u_img.at<uint8_t>(row, col/2) = static_cast<uint8_t>((value1 >>8) & 0xff);
      uint16_t value2 = quantized_img.at<uint16_t>(row, col + 1);
      y_img.at<uint8_t>(row, col + 1) = static_cast<uint8_t>(value2 & 0xff);
      v_img.at<uint8_t>(row, col/2) = static_cast<uint8_t>((value2 >>8) & 0xff);
    }
  }

}
}
void TestStreaming(int argc, char** argv) {

  std::vector<cv::Mat> yuv422_img(3);

  const float alpha = std::numeric_limits<uint8_t>::max() / (FLAGS_max_celcius - FLAGS_min_celcius);
  const float beta = -alpha * FLAGS_max_celcius;

  Streamer streamer;

  bool initialized = false;
  rosbag::Bag bag;
  bag.open(FLAGS_bag_filename, rosbag::bagmode::Read);
  rosbag::View view(bag);
  uint64_t start_timestamp;
  uint64_t last_timestamp;

  for (const rosbag::MessageInstance& message : view) {
    if (!::ros::ok()) {
      break;
    }
    if(!message.isType<sensor_msgs::Image>()) 
      continue;
    auto img_msg = message.instantiate<sensor_msgs::Image>();
    cv_bridge::CvImageConstPtr cv_ptr= cv_bridge::toCvCopy(*img_msg, sensor_msgs::image_encodings::TYPE_32FC1);
    uint64_t timestamp = img_msg->header.stamp.toNSec() / 1000000;

    if (!initialized) {
      const auto& input_image = cv_ptr->image;
      if (0 != streamer.Initialize(input_image.cols, input_image.rows, FLAGS_streaming_url, 5, 0)) {
        LOG(ERROR) << "Failed to initialize!";
        break;
      }
      yuv422_img[0]  = Mat(input_image.rows, input_image.cols, CV_8UC1, Scalar(0));
      yuv422_img[1]  = Mat(input_image.rows, input_image.cols / 2, CV_8UC1, Scalar(128));
      yuv422_img[2]  = Mat(input_image.rows, input_image.cols / 2, CV_8UC1, Scalar(128));
      start_timestamp = timestamp;
      last_timestamp = timestamp;
      initialized = true;
    }
    cv::Mat quantized_img;
    QuantizeTempertureImage(cv_ptr->image, quantized_img, FLAGS_min_celcius, FLAGS_max_celcius);
    ConvertQuantizedImageToYuv422(quantized_img, yuv422_img);
    

    std::this_thread::sleep_for(std::chrono::milliseconds(timestamp - last_timestamp));
    streamer.Send(yuv422_img, Eigen::Affine3d::Identity(), timestamp - start_timestamp);

  }

  streamer.Release();

}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  LOG(INFO) << "start test streamer .";

  ros::init(argc, argv, "rosbag_publisher");
  ros::start();
  streaming_service::TestStreaming(argc, argv);

  getchar();
 
}

