<!--
 * @Descripttion: 
 * @version: 
 * @Author: sujin
 * @Date: 2022-01-17 
 * @LastEditors: sujin
 * @LastEditTime: 2022-01-17 
-->
# undistort_service
接收红外驱动发出的温度图像以及定位服务提供的红外相机位姿，对两者进行时间同步，打包发送出位姿对齐的温度图像。 

##  订阅话题
###  红外温度图片
Topic: /fixed/infrared/raw 

Type: sensor_msgs::Image

##  发布话题
###  位姿对齐温度图片
Topic: /infrared/undistorted 

Type: undistort_service_msgs::PosedImage

undistort_service_msgs::PosedImage定义：
```json
sensor_msgs/Image image   #温度图片 
geometry_msgs/Pose pose   # 当前位姿 
sensor_msgs/CameraInfo camera_info  #相机参数
```

##  服务
###  设置相机参数
Service Name: undistort_service/update_calib_params  
Type: undistort_service_msgs::update_calib_params  

undistort_service_msgs::update_calib_params定义：
```json
string camera_id    #相机ID
string pose_id          #预置位ID
string[] calib_params   #相机标定参数
---
undistort_service_msgs/StatusResponse status
```
###  开始对齐任务
Service Name: undistort_service/start_undistort 

Type: undistort_service_msgs::start_undistort

```json
string camera_id    #相机ID
string pose_id          #预置位ID
---
undistort_service_msgs/StatusResponse status
```
###  停止对齐任务
Service Name: undistort_service/stop_undistort  

Type: undistort_service_msgs::stop_undistort 

undistort_service_msgs::stop_undistort定义：
```json
string camera_id    #相机ID
---
undistort_service_msgs/StatusResponse status
```


## Ros 参数:

| 名称                        | 类型        | 描述                        | 缺省值            |
|--------------------- |-----------|----------------------|------------------|
| raw_topic| int     | 红外图片消息       | /fixed/infrared/raw|
| undistort_topic| string     | 对齐图片消息的topic  |/infrared/undistorted|
| configuration_directory| string     | 配置文件保存目录  |undistort_service package dir/config|
| calib_filepath| string     |缺省标定文件  |undistort_service package dir/config/ir_lidar_calibration.yaml|

##  启动服务
###  启动着色对齐服务

```shell
#driver
roslaunch undistort_service undistort_service.launch
```
