#include <algorithm>
#include <fstream>
#include <iostream>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "undistort_node.h"

using namespace std;

namespace undistort_service {
void Run(int argc, char** argv) {
  LOG(INFO) << "start undistort service.";

  UndistortNode  undistort_node;


  ::ros::spin();

  LOG(INFO) << "finished  undistort servic.";


}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;

  ::ros::init(argc, argv, "undistort_service_node");
  
  undistort_service::Run(argc, argv);

}


