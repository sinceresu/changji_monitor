#include "undistort_node.h"
#include <limits>
#include <iostream>
#include <fstream>
#include <thread> 
#include <boost/filesystem.hpp>

#include "ros/package.h"
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include "tf2_eigen/tf2_eigen.h"

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "undistort_service/node_constants.h"

#include "camera.h"


using namespace std;
using namespace cv;
namespace fs = boost::filesystem;


namespace undistort_service {
constexpr double kTfBufferCacheTimeInSeconds = 10.;

namespace {
constexpr int kLatestOnlyPublisherQueueSize = 1;

// Subscribes to the 'topic' for 'trajectory_id' using the 'node_handle' and
// calls 'handler' on the 'node' to handle messages. Returns the subscriber.
template <typename MessageType>
::ros::Subscriber SubscribeWithHandler(
    void (UndistortNode::*handler)(const std::string&,
                                    const typename MessageType::ConstPtr&),
      const std::string& topic,
    ::ros::NodeHandle* const node_handle, UndistortNode* const node) {
  return node_handle->subscribe<MessageType>(
      topic, kLatestOnlyPublisherQueueSize,
      boost::function<void(const typename MessageType::ConstPtr&)>(
          [node, handler,
           topic](const typename MessageType::ConstPtr& msg) {
            (node->*handler)(topic, msg);
          }));
}
}

UndistortNode::UndistortNode() :
  buffer_(),
  listener_(buffer_),
  state_(UndistortState::IDLE),
  configuration_directory_(std::string(std::getenv("HOME"))  + "/undistort_service")
 {

  GetParameters();

  if(!fs::exists(configuration_directory_)) {
    fs::create_directories(configuration_directory_);
  }

  undistort_result_publisher_= node_handle_.advertise<undistort_service_msgs::PosedImage>(
        undistort_topic_, 1);    

  service_servers_.push_back(node_handle_.advertiseService(
      kStartUndistortServiceName, &UndistortNode::HandleStartUndistort, this));

  service_servers_.push_back(node_handle_.advertiseService(
      kStopUndistortServiceName, &UndistortNode::HandleStopUndistort, this));

  service_servers_.push_back(node_handle_.advertiseService(
      kUpdateCalibParamsServiceName, &UndistortNode::HandleUpdateCalibParams, this));

}

UndistortNode::~UndistortNode() {
  if (temp_image_subscriber_)  {
    temp_image_subscriber_.shutdown();
  }

 }

void UndistortNode::GetParameters() {
  
  ::ros::NodeHandle private_handle("~");

  private_handle.param<std::string>("/undistort_service/raw_topic", raw_topic_, "/fixed/infrared/raw");
  private_handle.param<std::string>("/undistort_service/undistort_topic", undistort_topic_, "/infrared/undistort");

   ros::param::get("~configuration_directory", configuration_directory_);

  ros::param::get("~calib_filepath", calibration_filepath_);

}



bool UndistortNode::HandleStartUndistort(
        undistort_service_msgs::start_undistort::Request& request,
        undistort_service_msgs::start_undistort::Response& response) {

  string calib_filepath = configuration_directory_ + "/" + request.camera_id + "_" + request.pose_id + ".yaml";
  if(!fs::exists(calib_filepath)) {
    LOG(WARNING) << "has not set calibration param for " << request.camera_id + ", pose_id: " +  request.pose_id;
    response.status.message = "has not set calibration param.";
    calib_filepath = calibration_filepath_;
  }
  {
    std::unique_lock<std::mutex> lock(mutex_);
    cameras_[request.camera_id] = make_shared<Camera>();
    cameras_[request.camera_id]->LoadCalibrationFile(calib_filepath);
  }

  temp_image_subscriber_ = SubscribeWithHandler<sensor_msgs::Image>(
            &UndistortNode::HandleTempImage, raw_topic_, &node_handle_,
            this);

  response.status.code = UndistortResult::SUCCESS;
  state_ = UndistortState::UNDISTORTING;
  return true;
}

bool UndistortNode::HandleStopUndistort(
        undistort_service_msgs::stop_undistort::Request& request,
        undistort_service_msgs::stop_undistort::Response& response) {


  bool camera_empty = false;
  {
    std::unique_lock<std::mutex> lock(mutex_);
    if (cameras_.count(request.camera_id) == 0) {
      response.status.code = UndistortResult::SUCCESS;
      response.status.message = "not undistorting.";
      LOG(WARNING) << "not undistorting! " ;
      return true;  
    }
    cameras_.erase(request.camera_id);
    camera_empty = cameras_.empty();
  }
  if (camera_empty) {
    temp_image_subscriber_.shutdown();
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
  }

  response.status.code = UndistortResult::SUCCESS;
  state_ = UndistortState::IDLE;
  return true;
}
      

bool UndistortNode::HandleUpdateCalibParams(
        undistort_service_msgs::update_calib_params::Request& request,
        undistort_service_msgs::update_calib_params::Response& response) {
  LOG(INFO) << "Enter HandleUpdateCalibParamsService . " ;

  string calib_filepath = configuration_directory_ + "/" + request.camera_id + "_" + request.pose_id + ".yaml";

  ofstream calib_file(calib_filepath);
  calib_file << request.calib_params[0];

  response.status.code = UndistortResult::SUCCESS;

  LOG(INFO) << "Exit HandleUpdateCalibParamsService . " ;
  return true;
}


void UndistortNode::HandleTempImage( const std::string& topic, const sensor_msgs::Image::ConstPtr& img_msg) {
  // ROS_INFO("HandleTempImage");
  ROS_INFO("HandleTempImage");
Eigen::Affine3d camera_pose;
{
    std::unique_lock<std::mutex> lock(mutex_);
    if (cameras_.count(img_msg->header.frame_id) == 0) {
      return;
    }
    camera_pose = Eigen::Affine3d(cameras_[img_msg->header.frame_id]->GetPose() );
}

  
  cv_bridge::CvImageConstPtr cv_ptr= cv_bridge::toCvCopy(*img_msg, sensor_msgs::image_encodings::TYPE_32FC1);
	// cv::Mat undistort_image;
  // camera_->Undistort(cv_ptr->image, undistort_image);

  cv_bridge::CvImage heat_cvimg(img_msg->header,
                            sensor_msgs::image_encodings::TYPE_32FC1, cv_ptr->image);

  undistort_service_msgs::PosedImage posed_image;
  
  cv_ptr->toImageMsg(posed_image.image);
  posed_image.image.header =  img_msg->header;
  posed_image.pose = Eigen::toMsg (camera_pose);

  undistort_result_publisher_.publish(posed_image);

}



}

