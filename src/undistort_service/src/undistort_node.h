#pragma once

#include <string>
#include <memory>
#include <vector>
#include <mutex>

#include "ros/ros.h"
#include <sensor_msgs/Image.h>
#include <tf/transform_listener.h>
#include "tf2_ros/transform_listener.h"

#include "undistort_service_msgs/PosedImage.h"

#include "undistort_service_msgs/update_calib_params.h"
#include "undistort_service_msgs/start_undistort.h"
#include "undistort_service_msgs/stop_undistort.h"



namespace  undistort_service{
class Camera;

class UndistortNode {
 public:
    enum UndistortResult {
        SUCCESS = 0,
        IO_ERROR = -2,
        INVALID_PARAM = -4,
        ERROR = -5
    };
  enum UndistortAction {
        START = 0,
        STOP = 1,
  };
  UndistortNode();
  ~UndistortNode();

  UndistortNode(const UndistortNode&) = delete;
  UndistortNode& operator=(const UndistortNode&) = delete;

  private:
    enum UndistortState {
        IDLE = 0,
        UNDISTORTING = 1,
    };

    void GetParameters();
    
    bool HandleStartUndistort(
        undistort_service_msgs::start_undistort::Request& request,
        undistort_service_msgs::start_undistort::Response& response);
    
    bool HandleStopUndistort(
        undistort_service_msgs::stop_undistort::Request& request,
        undistort_service_msgs::stop_undistort::Response& response);

    bool HandleUpdateCalibParams(
        undistort_service_msgs::update_calib_params::Request& request,
        undistort_service_msgs::update_calib_params::Response& response);

    void HandleTempImage( const std::string& topic, const sensor_msgs::Image::ConstPtr& image_msg);


    ::ros::NodeHandle node_handle_;
    ::ros::Publisher undistort_result_publisher_;
    std::vector<::ros::ServiceServer> service_servers_;

    ::ros::Subscriber temp_image_subscriber_;

    int state_ = UndistortState::IDLE;

    std::string raw_topic_;
    std::string calibration_filepath_;
    std::string undistort_topic_;

    std::string configuration_directory_;

    std::shared_ptr<Camera> camera_;
    tf2_ros::Buffer buffer_;
    tf2_ros::TransformListener listener_;
    
    std::map<std::string, std::shared_ptr<Camera> > cameras_;

    bool calib_file_updated_ = false;
    std::mutex mutex_;
  };
  
}