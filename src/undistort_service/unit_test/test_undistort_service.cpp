
#include <stdio.h>
#include <thread> 

#include <opencv2/opencv.hpp>

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Image.h>

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include "tf2_eigen/tf2_eigen.h"
#include <tf_conversions/tf_eigen.h>
#include "tf2_ros/transform_broadcaster.h"

#include "gflags/gflags.h"
#include "glog/logging.h"
DEFINE_string(topics, "",

              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");



#include <memory>
using namespace std;
namespace undistort_service{
namespace {

void Undistort(int argc, char** argv) {
    ::ros::NodeHandle node_handle_;

  Eigen::Affine3d camera_to_lidar = Eigen::Affine3d::Identity();
  geometry_msgs::TransformStamped transform_msg = tf2::eigenToTransform(camera_to_lidar);
  transform_msg.header.stamp = ros::Time::now();
  transform_msg.header.frame_id = "map";
  transform_msg.child_frame_id = "lidar_pose";
  tf2_ros::TransformBroadcaster broadcaster_;
  broadcaster_.sendTransform(transform_msg);

  std::this_thread::sleep_for(std::chrono::milliseconds(50));

	cv::Mat raw_image(384, 288, CV_32F, 10 );
  ::std_msgs::Header header;
  header.stamp = ros::Time::now();
  header.frame_id = "ir_raw";
  cv_bridge::CvImage heat_cvimg(header,
                                sensor_msgs::image_encodings::TYPE_32FC1, raw_image);
  ros::Publisher temperature_buffer_pub_  = node_handle_.advertise<sensor_msgs::Image>("/fixed/infrared/raw", 1);

  temperature_buffer_pub_.publish(heat_cvimg.toImageMsg());

  std::this_thread::sleep_for(std::chrono::milliseconds(50));
  transform_msg.header.stamp = ros::Time::now();
  broadcaster_.sendTransform(transform_msg);


  
  ::ros::spin();


}

}
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  LOG(INFO) << "start test_undistort_service.";

  ros::init(argc, argv, "test_undistort_service");
   ros::start();
  undistort_service::Undistort(argc, argv);

  getchar();
 
}

